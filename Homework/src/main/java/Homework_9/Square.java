package Homework_9;

public class Square extends Rectangle implements Moveable{

    public Square(int x, int y) {
        super(x, y);
    }

    public void move(int mX, int mY) {
        System.out.println("Старые координаты квадрата " + getX() + " " + getY());
        setX(mX);
        setY(mY);
        System.out.println("Измененные координаты квадрата " + mX + " " + mY);


    }

    @Override
    public int getPerimeter() {
        System.out.println("Периметр квадрата ");
        return getX() * 4;
    }
}
