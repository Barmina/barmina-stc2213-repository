package Homework_9;

import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random random = new Random();
        Figure[] figures = new Figure[4]; //Создаем массив с фигурами.
        Moveable[] moveables = new Moveable[2]; //Создаем массив с "перемещаемыми" фигурами.
//Заполняем массив фигурами, задаем им координаты. Выводим метод getPerimeter каждой фигуры в консоль.
        for (int i = 0; i < figures.length; i++) {
            figures[0] = new Rectangle(9, 5);
            figures[1] = new Ellipse(4, 6);
            figures[2] = new Circle(7, 10);
            figures[3] = new Square(8, 8);

            System.out.println(figures[i].getPerimeter());
        }
//Заполняем массив "перемещаемыми" фигурами
        for (int i = 0; i < moveables.length; i++) {
            moveables[0] = new Circle(7, 9);
            moveables[1] = new Square(4, 4);
            moveables[i].move(random.nextInt(50), random.nextInt(100));//Обращаемся к методу move,
            // у фигур рандомно изменяем координаты.


        }

    }
}

