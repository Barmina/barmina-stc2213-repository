package Homework_10;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        final int[] array = new int[5];
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(50) + 1;
            System.out.println(array[i]);
        }

        ByCondition conditionNumbers = new ByCondition() {
            public boolean isOk(int number) {
                return number % 2 == 0;
            }
        };
        ByCondition conditionOne = new ByCondition() {
            public boolean isOk(int number) {
                int sum = 0;
                while (number > 0) {
                    sum += number % 10;
                    number /= 10;

                }
                return sum % 2 == 0;


            }


        };

        System.out.println("Чётные числа ");
        printNumbers(array, conditionNumbers);
        System.out.println("Проверка суммы чисел на четность ");
        printNumbers(array, conditionOne);


    }


    public static void printNumbers(int[] array, ByCondition condition) {
        for (int i = 0; i < array.length; i++) {
            if (condition.isOk(array[i])) {
                System.out.println(array[i]);
            }
        }
    }

}
