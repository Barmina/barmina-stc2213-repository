package Homework1;
import java.util.Arrays;
public class Permutation {
    public static void main(String[] args) {
        int[] elements = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
        int swapCount = 0;
        int lastIndex = elements.length - 1;
        for (int i = lastIndex - 1; i >= 0; i--) {
            if (elements[i] == 0) {
                elements[i] = elements[lastIndex - swapCount];
                elements[lastIndex - swapCount] = 0;
                swapCount++;
            }
        }
        for (int i : elements) {
            System.out.print(i + ", ");
        }
    }
}