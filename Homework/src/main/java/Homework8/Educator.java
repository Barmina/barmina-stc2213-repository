package Homework8;

public class Educator extends Worker {
    public Educator(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }

    @Override
    public void goToWork() {
        System.out.println("Воспитатель работает по 6 часов в день.");
    }


    @Override
    public void goToVocation(int days) {
        System.out.println("Отпуск воспитателя " + days + " дней");

    }
}

