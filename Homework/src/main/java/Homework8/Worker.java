package Homework8;

/* Так же у класса Worker, есть два метода:
public void goToWork(); //Метод в котором описана работа. Метод, как минимум, должен выводить кто работает,
какая у него профессия и как он работает.
Каждый потомок должен переопределить этот метод (каждая профессия работает по своему)

public void goToVacation(int days); //Метод в котором описан отпуск. На вход принимает количество дней отпуска.
Метод, как минимум, должен выводить кто уходит в отпуск, какая у него профессия и на сколько дней уходит в отпуск.
        Каждый потомок должен переопределить этот метод (каждая профессия отдыхает по своему)

Нужно создать минимум четыре класса потомка (профессию выбирайте сами:)),
        например - Программист, Тестировщик, СисАдмин, ДевОпс и.т.д.
В классе Main (в котором создаются объекты всех классов профессий)
        через полиморфизм вывести отпуск и работу каждого объекта по аналогии с "машинами" на уроке. */
public class Worker {
    private String name;
    private String lastName;
    private String profession;

    public Worker(String name, String lastName, String profession) {
        this.name = name;
        this.lastName = lastName;
        this.profession = profession;
    }
    public void goToWork(){

    }
    public void goToVocation(int days) {

    }
    public String toString(){
         return name + lastName + profession;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }
}

