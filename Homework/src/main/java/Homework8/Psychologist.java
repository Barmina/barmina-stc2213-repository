package Homework8;

public class Psychologist extends Worker {
    public Psychologist(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }

    @Override
    public void goToWork() {
        System.out.println("Психолог работает по 7 часов в день.");
    }


    @Override
    public void goToVocation(int days) {
        System.out.println("Отпуск психолога " + days + " дней");
    }
}
