package Homework8;

public class Teacher extends Worker {
    public Teacher(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }

    @Override
    public void goToWork() {
        System.out.println("Учитель работает по 8 часов в день.");
    }


    @Override
    public void goToVocation(int days) {
        System.out.println("Отпуск учителя " + days + " дней");
    }
}